
class Board
  attr_reader :grid

  def self.default_grid
    Array.new(3) { Array.new(3) }
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def [](position)
    row, col = position
    @grid[row][col]
  end

  def []=(position, mark)
    row, col = position
    @grid[row][col] = mark
  end

  def place_mark(position, mark)
    self[position] = mark
  end

  def empty?(position)
    self[position].nil?
  end

  def winner
    return :X if victory?(:X)
    return :O if victory?(:O)
  end

  def victory?(mark)
    row_victory?(mark) || column_victory?(mark) ||
      diagonal_victory?(mark)
  end

  def row_victory?(mark)
    three_in_a_row?(grid, mark)
  end

  def column_victory?(mark)
    three_in_a_row?(grid.transpose, mark)
  end

  def three_in_a_row?(rows_or_columns, mark)
    rows_or_columns.any? { |row| row.all? { |space| space == mark } }
  end

  def diagonal_victory?(mark)
    left_diagonal.all? { |space| space == mark } ||
      right_diagonal.all? { |space| space == mark }
  end

  def left_diagonal
    diagonal = []

    grid.each_index do |idx|
      diagonal << grid[idx][idx]
    end

    diagonal
  end

  def right_diagonal
    diagonal = []
    column_idx = grid.size - 1

    grid.each_index do |row_idx|
      diagonal << grid[row_idx][column_idx]
      column_idx -= 1
    end

    diagonal
  end

  def full?
    grid.flatten.none?(&:nil?)
  end

  def over?
    full? || winner
  end

  def all_spaces
    spaces = []
    grid.each_index do |row_idx|
      grid.size.times do |col_idx|
        pos = [row_idx, col_idx]
        spaces << pos
      end
    end

    spaces
  end
end
