class HumanPlayer
  attr_accessor :mark
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def display(board)
    rows_for_display(board).each do |row|
      print "#{row.join(' | ')}\n"
    end
  end

  def get_move
    puts "\nWhere to mark? (row index, column index; ex '0, 1')"
    gets.chomp.split(', ').map(&:to_i)
  end

  private

  def rows_for_display(board)
    board.grid.map do |row|
      row.map do |space|
        space = '_' if space.nil?
        space
      end
    end
  end

end
