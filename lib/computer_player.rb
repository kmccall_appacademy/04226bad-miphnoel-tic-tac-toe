class ComputerPlayer
  attr_accessor :mark
  attr_reader :name, :board

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    winning_move ? winning_move : random_move
  end

  private

  def winning_move
    possible_moves.detect { |pos| winning_move?(pos) }
  end

  def random_move
    possible_moves.sample
  end

  def possible_moves
    board.all_spaces.select { |pos| board.empty?(pos) }
  end

  def winning_move?(pos)
    board[pos] = mark
    boolean_for_the_win = board.victory?(mark)
    board[pos] = nil
    boolean_for_the_win
  end
end
