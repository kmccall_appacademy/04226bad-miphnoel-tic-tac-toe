require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :board, :current_player, :player_one, :player_two

  def initialize(player_one, player_two, board = Board.new)
    @player_one = player_one
    @player_one.mark = :X
    @player_two = player_two
    @player_two.mark = :O
    @board = board
    @current_player = player_one
    @player_one.mark = :X

  end

  def play
    until board.over?
      play_turn
    end

    victory_report
  end

  def play_turn
    current_player.display(board)
    move = get_move
    board.place_mark(move, current_player.mark)
    switch_players!
  end

  def switch_players!
    @current_player = other_player
  end

  private

  def other_player
    current_player == player_one ? player_two : player_one
  end

  def get_move
    move = current_player.get_move
    valid_move?(move) ? move : try_again
  end

  def valid_move?(move)
    board.all_spaces.include?(move) && board.empty?(move)
  end

  def try_again
    puts "\nThat is not a valid move."
    get_move
  end

  def victory_report
    if board.winner
      puts "#{board.winner} won!"
    else
      puts "It's a tie"
    end
  end
end

# if __FILE__ == $PROGRAM_NAME
#   mike = HumanPlayer.new("mike")
#   comp = ComputerPlayer.new("comp")
#   game = Game.new(mike, comp, Board.new( Array.new(4) { Array.new(4) }))
#   game.play
# end
